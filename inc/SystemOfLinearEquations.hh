#pragma once

#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include <cmath>
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template < typename Type, int SIZE>
class SystemOfLinearEquations
{
  Matrix<Type,SIZE> factor; // factor - wspolczniniki
  Vector<Type,SIZE> vector_words_available; //wektor wyrazow wolnych
  Vector<Type,SIZE> result_vector; // wektor wyrazow szukanych
  Vector<Type,SIZE> error_vector; //dlugosc wektora bledu
  Type error; //blad oobliczen
public:

   Matrix<Type,SIZE> get_factor()const {return factor;} //funkcja zwracajaca wspolczynnik
   Vector<Type,SIZE> get_result_vector()const {return result_vector;} //funkcja zwracajaca wyrazy szukane
     Vector<Type,SIZE> get_error_vector()const {return error_vector;} //funkcja zwracajaca wektor bledu
       Type get_error()const {return error;} //funkcja zwracajaca blad obliczen
   Vector<Type,SIZE> get_vector_words_available()const {return vector_words_available;} //funkcja zwracajaca wektor wyrazow wolnych
   Matrix<Type,SIZE> &set_factor() {return factor;} //funkcja wczytujaca wspolczynniki
   Vector<Type,SIZE> &set_vector_words_available() {return vector_words_available;} //funkcja wczytujaca wyrazy wolna
   void Error(); 
   int calculate();
};

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy SystemOfLinearEquations, ktore zawieraja 
 *  wiecej kodu niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */
template < typename Type, int SIZE>
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations<Type,SIZE> &system) //przeciazenie wejscia dla dzialania
{
    stream>>system.set_factor()>>system.set_vector_words_available(); //wczytywanie macierzy najpierw wspolczynnikow nastepnie wektor wyrazow wolnych
    return stream;
}

template < typename Type, int SIZE>
std::ostream &operator<<(std::ostream &stream,const SystemOfLinearEquations<Type,SIZE> &system) // przeciazenie dla wyjscia
{
stream<<"A^T"<<std::endl; //macierz jest wczytywana w postaci transponowanej i zostaje wyswietlona w normalnej
    stream<<system.get_factor()<<std::endl;
    stream<<"vector words available"<<std::endl; // wyswietlenie  wektorow wyrazow wolnych
    stream<<system.get_vector_words_available()<<std::endl;
    return stream;

}

template < typename Type, int SIZE>
void SystemOfLinearEquations<Type,SIZE>::Error() //funkcja wyznaczjaca blad obliczeniowy
{
    error_vector=factor*result_vector-vector_words_available; //oblicznie dlugosci wektora bledu
   // error=sqrt(error_vector*error_vector); // wyliczenie bledu
}
template < typename Type, int SIZE>
   int SystemOfLinearEquations<Type,SIZE>::calculate() // obliczanie wyznacznikow
   {

     factor.Gauss();
     Matrix<Type,SIZE> tmp;
     Type determinant= factor.get_determinant();

     if(determinant==0)
     {
        for(int i=0; i<SIZE;i++)
        {
            tmp=factor;
            tmp[i]=vector_words_available;
            tmp.Gauss();
            if( tmp.get_determinant()==0) /// 0/0 nieskonczenie wiele rozewiazan
                return 0;
        }
        return 1; /// kiedy tylko wyznacznik glownej macierzy jest rowny 0 to nie ma rozwiazan
     }

        for(int i=0; i<SIZE;i++)
        {
            tmp=factor;
            tmp[i]=vector_words_available;
            tmp.Gauss();
            
            result_vector[i]=tmp.get_determinant()/determinant;
        }


     return 2; // gdy sa rozwiazania

   }