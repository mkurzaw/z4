#pragma once

#include "Size.hh"
#include <iostream>
#include "LZespolona.hh"
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */


template < typename Type, int SIZE>
class Vector
{
  Type tab[SIZE]; //tablica dla 3 wektorow
public:
Type & operator[](int i)
  {
    return tab[i];
  }

  Type operator[](int i) const
  {
    return tab[i];
  }
    //przeciazenia dzialan dla wektorow
       Vector<Type,SIZE> operator +( Vector<Type,SIZE> B);
       Vector<Type,SIZE> operator -( Vector<Type,SIZE> B);
       Type operator *( Vector<Type,SIZE> B);
       Vector<Type,SIZE> operator *( Type B);
       Vector<Type,SIZE> operator /( Type B);

/*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
};
template < typename Type, int SIZE>
std::istream& operator >> (std::istream &Strm, Vector<Type,SIZE> &Vec)// strumien wejsciowy dla wektora
{
    for(int i=0;i<SIZE; i++)
    Strm>>Vec[i];
    return Strm;
}
template < typename Type, int SIZE>
 Vector<Type,SIZE> Vector<Type,SIZE>::operator +( Vector<Type,SIZE> B) //przeciazenie dodawania
  {
    Vector<Type,SIZE> result;
    for(int i=0; i<SIZE; i++) // dla kazdej wartosci wektora wykonywane dodawanie
      result[i]=tab[i]+B[i];
    return result;
  }
template < typename Type, int SIZE>
   Vector<Type,SIZE> Vector<Type,SIZE>::operator -( Vector<Type,SIZE> B) //przeciazenie odj
  {
    Vector<Type,SIZE> result;
    for(int i=0; i<SIZE; i++)
      result[i]=tab[i]-B[i];
    return result;
  }
template < typename Type, int SIZE>
   Vector<Type,SIZE> Vector<Type,SIZE>::operator *( Type B) //przeciazenie mnoz
  {
    Vector<Type,SIZE> result;
    for(int i=0; i<SIZE; i++)
     result[i]=tab[i]*B;
    return result;
  }

template < typename Type, int SIZE>
     Type Vector<Type,SIZE>::operator *( Vector<Type,SIZE> B)
  {
    Type result=0;
    for(int i=0; i<SIZE; i++)
      result=result+tab[i]*B[i];
    return result;
  }
template < typename Type, int SIZE>
   Vector<Type,SIZE> Vector<Type,SIZE>::operator /( Type B) //przeciazenie dzielenia
  {
    Vector<Type,SIZE> result;
    for(int i=0; i<SIZE; i++)
      result[i]=tab[i]/B;
    return result;
  }

template < typename Type, int SIZE>
std::ostream& operator << (std::ostream &Strm, const Vector<Type,SIZE> &Vec) //przeciazenie wyjscia
{
    for(int i=0;i<SIZE; i++)
    Strm<<Vec[i]<<"\t"; // wyswietlaie wyrazow wolnych 
    return Strm;
}