#pragma once

#include "Size.hh"
#include "Vector.hh"
#include <iostream>
#include <algorithm>
/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template < typename Type, int SIZE>
class Matrix
{
  Vector<Type,SIZE> M[SIZE];
  Type determinant; //determinant-wyznacznik
  public:
   
   Type get_determinant() // funkcja wyznacznia wyznacznika
   {
    return determinant;
   }

    Vector<Type,SIZE> & operator[](int i) 
  {
    return M[i];
  }

  Vector<Type,SIZE> operator[](int i) const //przeciazenie operetora [] tak aby zwracal wartosc z pola klasy na zewnatrz
  {
    return M[i];
  }  

  void Gauss();
  
  Vector<Type,SIZE> operator * (Vector<Type,SIZE> B);
  
};
template < typename Type, int SIZE>
std::istream &operator>>(std::istream &stream, Matrix<Type,SIZE> &Matrix) //przeciazenie dla strumienia wejsciowego
{
    for(int i=0; i<SIZE; i++)
        stream>>Matrix[i];
    return stream;
}
template < typename Type, int SIZE>
std::ostream &operator<<(std::ostream &stream, const Matrix<Type,SIZE> &Matrix) //przeciazenie dla strumienia wyjsciowego
{
    for(int i=0; i<SIZE; i++)
        stream<<Matrix[i]<<std::endl;
    return stream; 
}

template < typename Type, int SIZE>
  void Matrix<Type,SIZE>::Gauss() //metoda oblicza wyznacznik metoda gaussa
  {
      Matrix<Type,SIZE> tmp=*this; /// this wskaznik na samego siebie kopiujemy tu macierz do macierzy pomocnieczej
      Type parity=1;
      for(int i=0; i<SIZE; i++)
      {
          for(int j=i+1; j<SIZE && tmp[i][i]==0; j++  )
          {
              parity=parity*-1;
              std::swap(tmp[i],tmp[j]);
          }
      
      if(tmp[i][i]==0)
      {
          determinant=0;
      }
      
      determinant=parity;
      for(int j=i+1; j<SIZE; j++)
      {
          tmp[j]= tmp[j]-tmp[i]*(tmp[j][i]/tmp[i][i]);
         
      } 
      }
 for(int i=0; i<SIZE; i++)
      determinant=determinant*tmp[i][i];
    

  }

template < typename Type, int SIZE>
    Vector<Type,SIZE> Matrix<Type,SIZE>::operator * (Vector<Type,SIZE> B)
    {
        Vector<Type,SIZE> result;
        for(int i=0; i<SIZE; i++)
        {
        result[i]=0;
            for(int j=0; j<SIZE; j++)
             result[i]= result[i]+M[j][i]*B[j];
        }
        return result;


    }