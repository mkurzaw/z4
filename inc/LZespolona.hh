#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>

using namespace std;

/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.
 */


/*!
 * Modeluje pojecie liczby zespolonej
 */
class  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */
  public:
  LZespolona(double re=0, double im=0)
  {
    this->re=re;
    this->im=im;
  }
  bool operator == (double Skl);
  double get_re() const {return re;}
  double get_im() const {return im;}
  double &set_re()  {return re;}
  double &set_im()  {return im;}
LZespolona  operator + ( LZespolona  Skl2);
LZespolona  operator - (  LZespolona  Skl2);
LZespolona  operator * (  LZespolona  Skl2);
LZespolona  operator / ( double Mod2);
LZespolona  operator / (  LZespolona  Skl2);
LZespolona Sprzezenie(LZespolona Skl1m);
double Modul2(LZespolona Skl2);
};


/*
 * Dalej powinny pojawic sie zapowiedzi definicji przeciazen operatorow
 */

ostream &operator <<(ostream& Str,LZespolona Skl1); //przeciazenie wyjscia
istream &operator >>(istream& Str, LZespolona &Skl1);//przeciazenie dla wejscia ampersand bo zmieniamy wartosc


#endif
