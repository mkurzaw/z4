#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */

int main()
{
  char char_;
  cin>>char_;
  if(char_=='z')
  {
  SystemOfLinearEquations<LZespolona,5> system; // dzialanie

  cout << endl
       << " Start programu " << endl;
  cin>>system; //wczytanie danych
  cout<<system<< endl; //wyswietlenie przeciazonego wyjscia
  cout<<"result"<<endl;
  switch (system.calculate())
  {
  case 0: // dla nieskonczenie wielu rozwiazan
   cout<<"nieskonczenie wiele rozwoazan"<<endl;
    break;
    case 1: //brak rozw
   cout<<"brak rozwiazan"<<endl;
    break;
  case 2: //dla 3 rozwiazan rzeczywistych
   cout<<system.get_result_vector()<<endl; 
   system.Error();
   cout<<"wektor bledu : "<<system.get_error_vector()<<endl;
    break;


  }
  }
  else  if(char_=='r')
  {
  SystemOfLinearEquations<double,5> system; // dzialanie

  cout << endl
       << " Start programu " << endl;
  cin>>system; //wczytanie danych
  cout<<system<< endl; //wyswietlenie przeciazonego wyjscia
  cout<<"result"<<endl;
  switch (system.calculate())
  {
  case 0: // dla nieskonczenie wielu rozwiazan
   cout<<"nieskonczenie wiele rozwoazan"<<endl;
    break;
    case 1: //brak rozw
   cout<<"brak rozwiazan"<<endl;
    break;
  case 2: //dla 3 rozwiazan rzeczywistych
   cout<<system.get_result_vector()<<endl; 
   system.Error();
   cout<<"wektor bledu : "<<system.get_error_vector()<<endl;
    break;


  }
  }
}
/* Mikolaj Kurzwski 02.05.2020Rozwiazywanie rowan liniowych nr indeksu:252894
Program ma za zadanie wczytac macierz wpolczynnikow rownan wielkosci 5x5a nastepnie wektor wyrazow wolnych. 
Macierze musza byc wczytane w postaci transponowanej.
Macierz moze zawierac wyrazenia zepolone.
Na wyjsciu programu pokaze sie macierz wspolczynniokow w postaci ogolnej, nastepnie zostana wyswietlone wyrazy wolne, nastepnie wyrazy szukane, a na koniec dlugosc wektora bledu i jego wartosc.
test:
Wprowadzone dane:
2.00 1.00 1.00 1.00 2.00
2.00 2.00 3.00 1.00 2.00
1.00 1.50 1.00 1.00 0.00
3.00 1.00 2.00 4.00 0.00
3.00 2.00 2.00 0.00 1.00
9.00 8.00 8.00 9.00 1.00
Dane wyswietlone:
A^T
2.00 1.00 1.00 1.00 2.00
2.00 2.00 3.00 1.00 2.00
1.00 1.50 1.00 1.00 0.00
3.00 1.00 2.00 4.00 0.00
3.00 2.00 2.00 0.00 1.00
vector words available
9.00 8.00 8.00 9.00 1.00

result
-0.13 0.47 3.51 1.29 0.31
wektor bledu : (-1.8e-15 -1.8e-15 -1.8e-15 0.0e+00 -2.9e-15)
test dla zespolonych:
Dane wprowadzone
(2.00+1.00i) (1.00+1.00i) (1.00+1.00i) (1.00+2.00i) (2.00+3.00i)
(2.00+3.00i) (2.00+5.00i) (3.00+7.00i) (1.00+2.00i) (2.00+2.00i)
(1.00+2.00i) (1.50+3.00i) (1.00+1.00i) (1.00+1.00i) (0.00+2.00i)
(3.00+1.00i) (1.00+4.00i) (2.00+1.00i) (4.00+3.00i) (0.00+0.00i)
(3.00+2.00i) (2.00+2.00i) (2.00+0.00i) (0.00+1.00i) (1.00+1.00i)
(9.00+1.00i) (8.00+2.00i) (8.00+8.00i) (9.00+5.00i) (1.00+1.00i)
Dane wyswietlone:
A^T
(2.00+1.00i) (1.00+1.00i) (1.00+1.00i) (1.00+2.00i) (2.00+3.00i)
(2.00+3.00i) (2.00+5.00i) (3.00+7.00i) (1.00+2.00i) (2.00+2.00i)
(1.00+2.00i) (1.50+3.00i) (1.00+1.00i) (1.00+1.00i) (0.00+2.00i)
(3.00+1.00i) (1.00+4.00i) (2.00+1.00i) (4.00+3.00i) (0.00+0.00i)
(3.00+2.00i) (2.00+2.00i) (2.00+0.00i) (0.00+1.00i) (1.00+1.00i)
vector words available
(9.00+1.00i) (8.00+2.00i) (8.00+8.00i) (9.00+5.00i) (1.00+1.00i)
result
(0.31+3.34i) (1.18-0.44i) (-3.90-5.31i) (3.38+0.02i) (-1.88+1.59i)

wektor bledu : ((3.6e-15+4.4e-16i) (5.3e-15-1.3e-15i) (3.6e-15+1.8e-15i)
(5.3e-15+0.0e+00i) (8.9e-15+2.0e-15i))


*/
