#include <iostream>
#include <math.h>
#include "LZespolona.hh"

using namespace std;

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  LZespolona::operator + (LZespolona  Skl2)//LZespolona::operator dla dodawania liczb zespolonych
{
  LZespolona  Wynik;

  Wynik.set_re() = re + Skl2.get_re();
  Wynik.set_im() = im + Skl2.get_im();
  return Wynik;
}
LZespolona  LZespolona::operator - (LZespolona  Skl2)//LZespolona::operator dla odejmowania
{
  LZespolona  Wynik;

  Wynik.set_re() = re - Skl2.get_re();
  Wynik.set_im() = im - Skl2.get_im();
  return Wynik;
}
LZespolona  LZespolona::operator * (LZespolona  Skl2)//LZespolona::operator dla mnozenia
{
  LZespolona  Wynik;

  Wynik.set_re() = re * Skl2.get_re() - im * Skl2.get_im();
  Wynik.set_im() = re * Skl2.get_im() + im * Skl2.get_re();
  return Wynik;
}
LZespolona LZespolona::operator / (double Mod2)// LZespolona::operator dla dzielenia
{
 LZespolona Wynik;
  Wynik.set_re() = re/Mod2;
  Wynik.set_im() = im/Mod2;
  return Wynik;

}
LZespolona  LZespolona::operator / ( LZespolona Skl2)
{
    LZespolona  Wynik;

  Wynik=*this*Skl2.Sprzezenie(Skl2)/ Skl2.Modul2(Skl2); //this - wskaznik na samego siebie obiek bierze samego siebie do obliczen
  return Wynik;
}

LZespolona LZespolona::Sprzezenie(LZespolona Skl1)
{
  Skl1.im=Skl1.im*(-1);
  return Skl1 ;

}

double LZespolona::Modul2(LZespolona Skl2)
{
  double Mod2;
  Mod2 = pow(Skl2.get_re(),2) + pow(Skl2.get_im(),2);
  return Mod2;
}

 
 bool LZespolona::operator == (double Skl)
 {
    if(re==Skl && im==0)
    return true;

    return false;
 }
 

ostream &operator <<(ostream& Str,LZespolona Skl1) //LZespolona::operator dla strumienia wyjsciowego 
{
   Str<<"("<<Skl1.get_re()<<showpos<<" "<<Skl1.get_im()<<noshowpos<<"i)";
   return Str;
}
istream &operator >>(istream& Str, LZespolona &Skl1) // LZespolona::operator dla wejscia 
{
     char znak ;
   Str>>znak;
   if(znak!='(')
   {
     Str.setstate(ios_base::badbit); // kiedy ten if sie wykona setstate ustawi statsu bitu bledu na true 
     return Str;
   }
   Str>>Skl1.set_re();
   if(Str.bad()) // funkcja bad() zwraca nam satus bitu bledu badbit true jesli popelnilismy blad a false gdy poszlo wszystko ok
   {
      return Str;
   }
      Str>>Skl1.set_im();
   if(Str.bad()) // funkcja bad() zwraca nam satus bitu bledu badbit true jesli popelnilismy blad a false gdy poszlo wszystko ok
   {
      return Str;
   }
   Str>>znak; 
   if (znak!='i')
   {
     Str.setstate(ios_base::badbit); // kiedy ten if sie wykona setstate ustawi statsu bitu bledu na true
     return Str;
   }
   Str>>znak;
   if (znak!=')')
   {
     Str.setstate(ios_base::badbit); // kiedy ten if sie wykona setstate ustawi statsu bitu bledu na true
     return Str;
   }
return Str;
}